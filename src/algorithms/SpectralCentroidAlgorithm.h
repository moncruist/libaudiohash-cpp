/*
 * SpectralCentroid.h
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#ifndef SPECTRALCENTROID_H_
#define SPECTRALCENTROID_H_

#include "STFTAlgorithm.h"

namespace audiohash {
    namespace algorithms {

        class SpectralCentroidAlgorithm: public STFTAlgorithm {
        public:
            virtual std::vector<double> * computeHash() override;
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* SPECTRALCENTROID_H_ */
