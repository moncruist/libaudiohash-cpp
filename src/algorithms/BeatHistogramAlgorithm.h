/**
 * @file BeatHistogramAlgorithm.h
 * @brief Алгоритм вычисления гистограммы темпов.
 */


#ifndef BEATHISTOGRAMALGORITHM_H_
#define BEATHISTOGRAMALGORITHM_H_

#include "SpecialAlgorithm.h"
#include <vector>
#include <map>

namespace audiohash {
    namespace algorithms {

        /**
         * Класс вычисления гистограммы темпов.
         */
        class BeatHistogramAlgorithm: public SpecialAlgorithm {
        public:
            BeatHistogramAlgorithm();
            virtual std::vector<double> *computeHash() override;
            virtual ~BeatHistogramAlgorithm();

        private:
            void fullWaveRectification(double *band, int bandLength);
            void lowPassFiltering(double *array, int length);
            std::vector<double> *downSampling(double *array, int length, int k);
            void meanRemoving(std::vector<double> &vec);
            std::vector<double> *autoCorrelation(std::vector<double> &vec);
            std::vector<std::pair<double, double> > *getPeaks(
                    std::vector<double> &vec, int peaksNum, int sampleRate);
            double hertzToBpm(double hertz);
            double BpmToHertz(double bpm);
            std::vector<double> *extractFeatures(double *histogram, unsigned int length);
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* BEATHISTOGRAMALGORITHM_H_ */
