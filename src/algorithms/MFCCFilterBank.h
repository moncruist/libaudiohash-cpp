/**
 * @file MFCCFilterBank.h
 */

#ifndef MFCCFILTERBANK_H_
#define MFCCFILTERBANK_H_

#include <vector>
#include <cmath>

namespace audiohash {
    namespace algorithms {

        /**
         * @brief Класс для генерации банка фильтров для MFCC.
         * @details При вычислении MFCC требуется генерировать
         * банк треугольных фильтров по количеству коэффициентов.
         * Данный класс как раз и занимается генерацией данных фильтров,
         * попутно производя кэширование результатов для ускорения.
         * Дело в том, что очень часто от окна к окну фильтры одни и те
         * же и было бы очень накладно каждый раз и генерировать.
         */
        class MFCCFilterBank {
        public:
            static std::vector<double *> &generateFilterBank(
                    unsigned int filtersNum, unsigned int filterLength);

            /**
             * Функция перевода из герцов в мелы.
             * @param hertz Частота в герцах
             * @return Частота в мелах
             */
            static inline double hertzToMel(double hertz) {
                return 2595.0L * std::log10(1.0L + hertz / 700.0L);
            }

            /**
             * Функция перевода из мелов в герцы
             * @param mel Частота в мелах
             * @return Частота в герцах
             */
            static inline double melToHertz(double mel) {
                return 700.0L * (std::pow(10, mel / 2595.0L) - 1);
            }
        private:
            static void reinitFilterBank(unsigned int filtersNum,
                    unsigned int filterLength);
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* MFCCFILTERBANK_H_ */
