/*
 * STFTAlgorithm.cpp
 *
 */

#include "STFTAlgorithm.h"

using namespace std;

namespace audiohash {
    namespace algorithms {

        STFTAlgorithm::STFTAlgorithm() :
            _fftData(nullptr), _fftDataLength(0), _frameRate(0), _frameDurationMs(0) {
        }

        STFTAlgorithm::~STFTAlgorithm() {
        }

        complex<double> const *STFTAlgorithm::getFFTData() const {
            return _fftData;
        }

        /**
         * Установка данных для алгоритма.
         * @param fftData Данные БПФ
         * @param fftDataLength длина буфера данных
         * @param frameRate Частота дискретезации для БПФ
         * @param frameDurationMs Длина фрейма для в миллисекундах
         */
        void STFTAlgorithm::setData(const std::complex<double>* fftData,
                unsigned long int fftDataLength,
                unsigned long int frameRate,
                unsigned long int frameDurationMs) {
            this->_fftData = fftData;
            this->_fftDataLength = fftDataLength;
            this->_frameRate = frameRate;
            this->_frameDurationMs = frameDurationMs;
        }

        unsigned long int STFTAlgorithm::getFFTDataLength() const {
            return _fftDataLength;
        }

        unsigned long int STFTAlgorithm::getFrameRate() const {
            return _frameRate;
        }

        unsigned long int STFTAlgorithm::getFrameDurationMs() const {
            return _frameDurationMs;
        }

    } /* namespace algorithms */
} /* namespace audiohash */
