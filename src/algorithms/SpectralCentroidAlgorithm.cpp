/*
 * SpectralCentroid.cpp
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#include "SpectralCentroidAlgorithm.h"
#include <complex>
#include <iostream>
#include <vector>

using namespace std;

namespace audiohash {
    namespace algorithms {

        vector<double>* SpectralCentroidAlgorithm::computeHash() {
            complex<double> const *fftData = getFFTData();
            unsigned long int fftDataLength = getFFTDataLength();
            unsigned long int frameRate = getFrameRate();
            unsigned long int duration = getFrameDurationMs();

            // Восстанавливаем длину фрейма в сыром виде
            unsigned int rawDataFrameLength = frameRate * duration / 1000;
            // Строим отношение длины сырых данных к длине данных после фурье
            double lengthRatio = (double)rawDataFrameLength / (double) fftDataLength;

            double fftFrameRate = double(frameRate) / lengthRatio;

            static int idxx = 0;

            vector<double> *hash = new vector<double>();
            double upper_sum = 0.0;
            double bottom_sum = 0.0;
            for(unsigned long int i = 0; i < fftDataLength / 2; i++) {
                upper_sum += abs(fftData[i]) * i;
                bottom_sum += abs(fftData[i]);
            }

            int result = (upper_sum / bottom_sum) * fftFrameRate / fftDataLength;

            if(idxx==10) {
                cout << "Centroid:" << upper_sum / bottom_sum << " " << result << endl;
            }
            idxx++;

            hash->push_back(upper_sum / bottom_sum);

            return hash;
        }

    } /* namespace algorithms */
} /* namespace audiohash */
