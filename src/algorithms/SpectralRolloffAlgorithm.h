/*
 * SpectralRolloffAlgorithm.h
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#ifndef SPECTRALROLLOFFALGORITHM_H_
#define SPECTRALROLLOFFALGORITHM_H_

#include "STFTAlgorithm.h"

namespace audiohash {
    namespace algorithms {

        class SpectralRolloffAlgorithm: public STFTAlgorithm {
        public:
            virtual std::vector<double> * computeHash() override;
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* SPECTRALROLLOFFALGORITHM_H_ */
