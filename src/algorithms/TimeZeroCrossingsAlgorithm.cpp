/*
 * TimeZeroCrossingsAlgorithm.cpp
 *
 *  Created on: 17.04.2013
 *      Author: moncruist
 */

#include "TimeZeroCrossingsAlgorithm.h"

#include <cmath>
using namespace std;

namespace audiohash {
    namespace algorithms {

        TimeZeroCrossingsAlgorithm::TimeZeroCrossingsAlgorithm() {
        }

        TimeZeroCrossingsAlgorithm::~TimeZeroCrossingsAlgorithm() {
        }

        /**
         * Данная функция вычисляет количество переходов через ноль у сигнала.
         * @return Количество переходов через ноль.
         */
        vector<double>* TimeZeroCrossingsAlgorithm::computeHash() {
            const double *data = getTimeData();
            unsigned long int length = getTimeLenght();

            double sum = 0.0;
            for(unsigned long int i = 1; i < length; i++) {
                int signA = (data[i] >= 0) ? 1 : 0;
                int signB = (data[i-1] >= 0) ? 1 : 0;
                sum += abs(signA - signB);
            }
            sum /= 2;
            vector<double> *hash = new vector<double>();
            hash->push_back(sum);

            return hash;
        }

    } /* namespace algorithms */
} /* namespace audiohash */
