/*
 * TimeDomainAlgorithm.h
 *
 *  Created on: 17.04.2013
 *      Author: moncruist
 */

#ifndef TIMEDOMAINALGORITHM_H_
#define TIMEDOMAINALGORITHM_H_

#include <vector>

namespace audiohash {
    namespace algorithms {

        /**
         * Базовый класс для алгоритмов во временном домене.
         */
        class TimeDomainAlgorithm {
        public:
            TimeDomainAlgorithm();
            virtual ~TimeDomainAlgorithm();

            void setData(double const *data, unsigned long length,
                    unsigned int durationMs);
            double const *getTimeData() const;
            unsigned long getTimeLenght() const;
            unsigned int getTimeDurationMs() const;

            virtual std::vector<double> *computeHash() = 0;

        private:
            double const *timeData;         //!< Данные во временном домене
            unsigned long timeLength;       //!< Длина буфера данных
            unsigned int timeDurationMs;    //!< Длина данных в миллисекундах
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* TIMEDOMAINALGORITHM_H_ */
