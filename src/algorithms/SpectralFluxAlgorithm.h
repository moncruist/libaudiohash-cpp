/*
 * SpectralFluxAlgorithm.h
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#ifndef SPECTRALFLUXALGORITHM_H_
#define SPECTRALFLUXALGORITHM_H_

#include "STFTAlgorithm.h"
#include <vector>

namespace audiohash {
    namespace algorithms {

        class SpectralFluxAlgorithm: public audiohash::algorithms::STFTAlgorithm {
        public:
            SpectralFluxAlgorithm();
            virtual std::vector<double> * computeHash() override;
            virtual ~SpectralFluxAlgorithm();

        private:
            std::vector<double> *previousNormalizedMagnitudes; //Массив нормализованных амплитуд прошлого фрейма
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* SPECTRALFLUXALGORITHM_H_ */
