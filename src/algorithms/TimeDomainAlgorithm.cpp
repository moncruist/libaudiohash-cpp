/*
 * TimeDomainAlgorithm.cpp
 *
 *  Created on: 17.04.2013
 *      Author: moncruist
 */

#include "TimeDomainAlgorithm.h"

namespace audiohash {
    namespace algorithms {

        TimeDomainAlgorithm::TimeDomainAlgorithm() :
            timeData(nullptr), timeLength(0), timeDurationMs(0) {
        }

        TimeDomainAlgorithm::~TimeDomainAlgorithm() {
        }

        void TimeDomainAlgorithm::setData(const double* data,
                unsigned long length, unsigned int durationMs) {
            timeData = data;
            timeLength = length;
            timeDurationMs = durationMs;
        }

        const double* TimeDomainAlgorithm::getTimeData() const {
            return timeData;
        }

        unsigned long TimeDomainAlgorithm::getTimeLenght() const {
            return timeLength;
        }

        unsigned int TimeDomainAlgorithm::getTimeDurationMs() const {
            return timeDurationMs;
        }

    } /* namespace algorithms */
} /* namespace audiohash */
