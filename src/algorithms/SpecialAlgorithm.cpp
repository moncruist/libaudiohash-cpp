/*
 * SpecialAlgorithm.cpp
 */

#include "SpecialAlgorithm.h"

namespace audiohash {
    namespace algorithms {

        SpecialAlgorithm::SpecialAlgorithm() :
            audioData(nullptr) {
        }

        void SpecialAlgorithm::setData(WindowedAudioData* data) {
            audioData = data;
        }

        WindowedAudioData* SpecialAlgorithm::getData() const {
            return audioData;
        }

        SpecialAlgorithm::~SpecialAlgorithm() {
        }

    } /* namespace algorithms */
} /* namespace audiohash */
