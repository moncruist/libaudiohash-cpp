/**
 * @file STFTAlgorithm.h
 */

#ifndef STFTALGORITHM_H_
#define STFTALGORITHM_H_
#include <complex>
#include <vector>

namespace audiohash {
    namespace algorithms {

        /**
         * Базовый класс для всех алгоритмов на основе БПФ.
         */
        class STFTAlgorithm {
        public:
            STFTAlgorithm();
            virtual ~STFTAlgorithm();

            void setData(std::complex<double> const * fftData,
                    unsigned long int fftDataLength,
                    unsigned long int frameRate,
                    unsigned long int frameDurationMs);

            std::complex<double> const * getFFTData() const;
            unsigned long int getFFTDataLength() const;
            unsigned long int getFrameRate() const;
            unsigned long int getFrameDurationMs() const;

            virtual std::vector<double> * computeHash() = 0;
        private:
            std::complex<double> const *_fftData;   //!< Данные БПФ
            unsigned long int _fftDataLength;       //!< длина буфера данных
            unsigned long int _frameRate;           //!< Частота дискретезации для БПФ
            unsigned long int _frameDurationMs;     //!< Длина фрейма для в миллисекундах
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* STFTALGORITHM_H_ */
