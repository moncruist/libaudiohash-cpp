/*
 * TimeZeroCrossingsAlgorithm.h
 *
 *  Created on: 17.04.2013
 *      Author: moncruist
 */

#ifndef TIMEZEROCROSSINGSALGORITHM_H_
#define TIMEZEROCROSSINGSALGORITHM_H_

#include "TimeDomainAlgorithm.h"
#include <vector>

namespace audiohash {
    namespace algorithms {

        /**
         * Данный класс считает количество переходов через ноль у сигнала.
         */
        class TimeZeroCrossingsAlgorithm: public TimeDomainAlgorithm {
        public:
            TimeZeroCrossingsAlgorithm();
            virtual ~TimeZeroCrossingsAlgorithm();
            virtual std::vector<double> *computeHash() override;
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* TIMEZEROCROSSINGSALGORITHM_H_ */
