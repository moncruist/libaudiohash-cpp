/**
 * @file SpecialAlgorithm.h
 * Абстрактный класс для специальных алгоритмов
 */

#ifndef SPECIALALGORITHM_H_
#define SPECIALALGORITHM_H_

#include <vector>
#include "../WindowedAudioData.h"

namespace audiohash {
    namespace algorithms {

        class SpecialAlgorithm {
        public:
            SpecialAlgorithm();
            void setData(WindowedAudioData *data);
            WindowedAudioData *getData() const;
            virtual std::vector<double> *computeHash() = 0;
            virtual ~SpecialAlgorithm();

        private:
            WindowedAudioData *audioData;
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* SPECIALALGORITHM_H_ */
