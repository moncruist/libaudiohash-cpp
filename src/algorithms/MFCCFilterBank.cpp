/**
 * @file MFCCFilterBank.cpp
 */

#include "MFCCFilterBank.h"

#include <cmath>
#include <cstring>
#include <iostream>
using namespace std;

namespace audiohash {
    namespace algorithms {

        /**
         * Кэш банка фильтров
         */
        struct MFCCFilterBankCache {
            vector<double *> *cache;    //!< Кэш фильтрова
            unsigned int filtersNum;    //!< Количество фильтров в кэше
            unsigned int filterLength;  //!< Длина фильтров в кэше
         };

        static MFCCFilterBankCache currentCache = {nullptr, 0, 0}; //!< Текущий кэш фильтрова

        /**
         * @brief Функция генерации банка фильтров.
         * @details Данная функция генерует новый банк фильтров,
         * если текущий кэш отличается от требуемого
         * @param filtersNum Количество фильтров
         * @param filterLength Длина фильтров
         * @return Банк фильтров
         */
        vector<double*> &MFCCFilterBank::generateFilterBank(
                unsigned int filtersNum, unsigned int filterLength) {

            // Проверяем, соответствует ли текущий кэш требуемому
            if(currentCache.filtersNum != filtersNum ||
                    currentCache.filterLength != filterLength) {
                // Кэш не соотвествует, необходимо пересоздать его
                if(currentCache.cache != nullptr) {
                    for (int i = 0; i < currentCache.cache->size(); i++) {
                        delete[] currentCache.cache->at(i);
                    }
                } else {
                    currentCache.cache = new vector<double *>();
                }
                currentCache.cache->clear();
                currentCache.filtersNum = filtersNum;
                currentCache.filterLength = filterLength;
                reinitFilterBank(filtersNum, filterLength);
            }
            return *currentCache.cache;
        }

        /**
         * Функция создания банка фильтров.
         * @param filtersNum Количество фильтров
         * @param filterLength Длина фильтров
         */
        void MFCCFilterBank::reinitFilterBank(unsigned int filtersNum,
                unsigned int filterLength) {

            const double minHertz = 0.0L;       // Минимальная частота в фильтрах
            const double maxHertz = 20000.0L;   // Максимальная частота в фильтрах

            // Переводим их в мелы
            double minMel = hertzToMel(minHertz);
            double maxMel = hertzToMel(maxHertz);

            // Вычисляем центры фильтров
            int range = filtersNum + 2;
            double *melCenters = new double[range];
            double *hertzCenters = new double[range];
            for(int i = 0; i < range; i++) {
                melCenters[i] = i * (maxMel - minMel) / ((double)(filtersNum + 1)) + minMel;
                hertzCenters[i] = melToHertz(melCenters[i]);
            }

            // Считаем, что в точке filterLength - 20kHz, а в 0 - 0Hz
            for(int i = 0; i < filtersNum; i++) {
                // Получаем края треугольного фильтра
                double start = hertzCenters[i];
                double center = hertzCenters[i+1];
                double end = hertzCenters[i+2];

                // Преобразовываем герцы в индексы
                int start_idx = int(start * filterLength / maxHertz);
                int center_idx = int(center * filterLength / maxHertz);
                int end_idx = int(end * filterLength / maxHertz);

                // Подготавливаем фильтр
                double *filter = new double[filterLength];
                memset(filter, 0, sizeof(double) * filterLength);

                // Строим фильтр
                double delta_up = 1.0L / double(center_idx - start_idx);
                double delta_down = 1.0L / double(end_idx - center_idx);

                double value = 0.0;
                for(int j = start_idx; j < center_idx; j++) {
                    filter[j] = value;
                    value += delta_up;
                }

                value = 1.0;
                for(int j = center_idx; j < end_idx; j++) {
                    filter[j] = value;
                    value -= delta_down;
                }

                currentCache.cache->push_back(filter);
            }
        }

    } /* namespace algorithms */
} /* namespace audiohash */

