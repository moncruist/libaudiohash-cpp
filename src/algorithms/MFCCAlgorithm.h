/**
 * @file MFCCAlgorithm.h
 * @brief Алгоритм вычисления MFCC.
 */

#ifndef MFCCALGORITHM_H_
#define MFCCALGORITHM_H_

#include <complex>
#include <vector>
#include "STFTAlgorithm.h"

namespace audiohash {
    namespace algorithms {

        /**
         * Класс для вычисления мел частотных кепстральных коэффициентов (MFCC).
         */
        class MFCCAlgorithm: public STFTAlgorithm {
        public:
            MFCCAlgorithm(unsigned int coefficientsCount);

            virtual std::vector<double> * computeHash() override;
            virtual ~MFCCAlgorithm();

            const int maxFrequency = 20000;     //!< Максимальная частота, которую рассматривает алгоритм

        private:
            unsigned int coeffCount;    //!< количество коэффициентов для рассчета
        };

    } /* namespace algorithms */
} /* namespace audiohash */
#endif /* MFCCALGORITHM_H_ */
