/*
 * SpectralRolloffAlgorithm.cpp
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#include "SpectralRolloffAlgorithm.h"
#include <vector>
#include <complex>
#include <iostream>

using namespace std;


namespace audiohash {
    namespace algorithms {

        vector<double>* SpectralRolloffAlgorithm::computeHash() {
            complex<double> const *fftData = getFFTData();
            unsigned long int fftDataLength = getFFTDataLength();
            unsigned long int frameRate = getFrameRate();
            unsigned long int duration = getFrameDurationMs();

            // Восстанавливаем длину фрейма в сыром виде
            unsigned int rawDataFrameLength = frameRate * duration / 1000;
            // Строим отношение длины сырых данных к длине данных после фурье
            double lengthRatio = (double)rawDataFrameLength / (double) fftDataLength;

            double fftFrameRate = double(frameRate) / lengthRatio;


           // int maxFreqIdx = maxFrequency * fftDataLength / fftFrameRate;

            static int idxx = 0;
            vector<double> *hash = new vector<double>();

            double sumAllMagnitude = 0.0;
            for(unsigned long int i = 0; i < fftDataLength / 2; i++) {
                sumAllMagnitude += abs(fftData[i]);
            }


            double magnitudeDistribution = 0.85 * sumAllMagnitude;
            double sum = 0.0;
            unsigned long int idx = 0;
            while(sum < magnitudeDistribution) {
                sum += abs(fftData[idx]);
                idx++;
            }
            int result = idx * fftFrameRate / fftDataLength;
            if(idxx == 10) {
                cout << "Rolloff: " <<idx<< " " << result <<endl;
            }
            idxx++;
            hash->push_back((double)idx);
            return hash;
        }

    } /* namespace algorithms */
} /* namespace audiohash */

