/**
 * @file MFCCAlgorithm.cpp
 * @brief Алгоритм вычисления MFCC.
 */

#include "MFCCAlgorithm.h"
#include "MFCCFilterBank.h"

#include <iostream>
#include <complex>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;


namespace audiohash {
    namespace algorithms {

        MFCCAlgorithm::MFCCAlgorithm(unsigned int coefficietnsCount) :
                coeffCount(coefficietnsCount) {
        }

        /**
         * Фукнция вычисления MFCC
         * @return Коэффиценты MFCC
         */
        std::vector<double>* MFCCAlgorithm::computeHash() {
            static long long idxx = 0;
            const complex<double> * fftData = getFFTData();
            unsigned long int fftDataLength = getFFTDataLength();
            unsigned long int frameRate = getFrameRate();
            unsigned long int duration = getFrameDurationMs();

            // Восстанавливаем длину фрейма в сыром виде
            unsigned int rawDataFrameLength = frameRate * duration / 1000;
            // Строим отношение длины сырых данных к длине данных после фурье
            double lengthRatio = (double)rawDataFrameLength / (double) fftDataLength;

            double fftFrameRate = double(frameRate) / lengthRatio;


            int maxFreqIdx = maxFrequency * fftDataLength / fftFrameRate;


            // Берем мощность сигнала
            double *windowFFTAbs = new double[maxFreqIdx];
            for (int i = 0; i < maxFreqIdx; i++) {
                double abs_value = abs(fftData[i]);
                abs_value *= abs_value;
                windowFFTAbs[i] = abs_value;
            }

            if (idxx == 10) {
                cout << "Spectre:" << endl;
                for (int i = 0; i < fftDataLength; i++) {
                    cout << abs(fftData[i]) << " ";
                }
                cout << endl;
            }
            // Готовим фильтры
            auto filters = MFCCFilterBank::generateFilterBank(coeffCount, maxFreqIdx);

            // Фильтруем сигнал с последующим сложением и получением логарифма
            vector<double> filteredLogWindows;
            for(double *filter : filters) {
                double sum = 0.0;
                for(int i = 0; i < maxFreqIdx; i++) {
                    sum += windowFFTAbs[i] * filter[i];
                }
                filteredLogWindows.push_back(log(sum));
            }

            auto coeffs = new vector<double>();
            // Дискретное косинусовое преобразование
            for(int i = 0; i < coeffCount; i++) {
                double sum = 0.0;
                for(int j = 0; j < filteredLogWindows.size(); j++) {
                    double spectre = filteredLogWindows[j];
                    sum += spectre * cos(M_PI * (i + 1) * double(j + 0.5) / double(coeffCount));
                }
                coeffs->push_back(sum);
            }

            if(idxx == 10) {
                cout << "MFCC:" << endl;
                for(auto coef : *coeffs) {
                    cout << coef << " ";
                }
                cout << endl;
            }

            idxx++;
            // прибираемся за собой
            delete[] windowFFTAbs;

            return coeffs;
        }

        MFCCAlgorithm::~MFCCAlgorithm() {
        }

    } /* namespace algorithms */
} /* namespace audiohash */
