/*
 * SpectralFluxAlgorithm.cpp
 *
 *  Created on: 15.04.2013
 *      Author: moncruist
 */

#include "SpectralFluxAlgorithm.h"
#include <complex>
#include <vector>
#include <iostream>

using namespace std;

namespace audiohash {
    namespace algorithms {

        SpectralFluxAlgorithm::SpectralFluxAlgorithm() :
                previousNormalizedMagnitudes(nullptr) {
        }

        SpectralFluxAlgorithm::~SpectralFluxAlgorithm() {
            delete previousNormalizedMagnitudes;
        }

        vector<double>* SpectralFluxAlgorithm::computeHash() {
            complex<double> const *fftData = getFFTData();
            unsigned long int fftDataLength = getFFTDataLength();

            vector<double> *hash = new vector<double>();

            vector<double> *normalizedMagnitudes = new vector<double>();
            double maxMagnitude = 0.0;
            for(unsigned long int i = 0; i < fftDataLength / 2; i++) {
                double magnitude = abs(fftData[i]);
                if(maxMagnitude < magnitude)
                    maxMagnitude = magnitude;

                normalizedMagnitudes->push_back(magnitude);
            }

            for(double &mag : *normalizedMagnitudes) {
                mag = mag / maxMagnitude;
            }

            if(previousNormalizedMagnitudes) {
                unsigned long int minLength = std::min(previousNormalizedMagnitudes->size(),
                        normalizedMagnitudes->size());
                double sum = 0.0;
                for(unsigned long int i = 0; i < minLength; i++) {
                    double distance = normalizedMagnitudes->at(i)
                            - previousNormalizedMagnitudes->at(i);

                    sum += distance * distance;
                }
                hash->push_back(sum);

                delete previousNormalizedMagnitudes;

            } else {
                hash->push_back(0.0);
            }

            previousNormalizedMagnitudes = normalizedMagnitudes;

            return hash;
        }



    } /* namespace algorithms */
} /* namespace audiohash */
