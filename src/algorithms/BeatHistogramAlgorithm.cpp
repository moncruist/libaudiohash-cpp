/**
 * @file BeatHistogramAlgorithm.cpp
 * @brief Алгоритм вычисления гистограммы темпов.
 */


#include "BeatHistogramAlgorithm.h"
#include "../WindowedAudioData.h"
#include "../DWTDaubechiesWrapper.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

namespace audiohash {
    namespace algorithms {

        const int dwtDecompLevel = 7; //!< количество уровней декомпозиции DWT

        BeatHistogramAlgorithm::BeatHistogramAlgorithm() {
        }

        vector<double>* BeatHistogramAlgorithm::computeHash() {
            vector<double> *result;
            // Инициализация данных
            WindowedAudioData *workData = getData();
            int windowLength = workData->windowLength();
            int windowsCount = workData->windowsCount();

            int sampleRate = workData->sampleRate();

            DWTDaubechiesWrapper dwt(windowLength, dwtDecompLevel);

            // Максимальное количество уровней
            int levelsNum = (int)log2((double)windowLength);

            double maxFreq = sampleRate / 2.0;

            int histLen = 160; //!< длина гистограммы
            double *hist = new double[histLen]; //!< гистограмма
            memset(hist, 0, sizeof(double) * histLen);

            // Для каждого окна прогоняем алгоритм
            for(int windowIdx = 0; windowIdx < windowsCount; windowIdx++) {
                double *samples = workData->data()[windowIdx];

                // Фильтруем данные
                auto subBands = dwt.filter(samples);

                int bandIdx = 0;
                // Каждый отфильтрованный канал обработываем отдельно
                for(auto band : subBands) {

                    // Считаем гистограмму темпа
                    fullWaveRectification(band, windowLength);

                    lowPassFiltering(band, windowLength);

                    auto downsampled = downSampling(band, windowLength, 16);

                    meanRemoving(*downsampled);

                    auto autocor = autoCorrelation(*downsampled);
                    delete downsampled;

                    // Берем пики значений
                    auto peaks = getPeaks(*autocor, 5, sampleRate / 16);

                    // Добавляем их на гистограмму
                    for(auto v : *peaks) {
                        int idx = v.second - 40;
                        hist[idx] += v.first;
                    }

                    bandIdx++;
                    delete autocor;
                    delete peaks;
                    delete[] band;
                }

                subBands.clear();
            }

            double overallSum = 0.0;
            for(int i = 0; i < histLen; i++)
                overallSum += hist[i];

            for(int i = 0; i < histLen; i++)
                hist[i] = hist[i] * 100.0 / overallSum;

            // Вычисляем признаки из гистограммы
            result = extractFeatures(hist, histLen);

            delete[] hist;

            return result;
        }

        BeatHistogramAlgorithm::~BeatHistogramAlgorithm() {
            // TODO Auto-generated destructor stub
        }

        void BeatHistogramAlgorithm::fullWaveRectification(double *band, int bandLength) {
            for(int i = 0; i < bandLength; i++) {
                band[i] = fabs(band[i]);
            }
        }

        void BeatHistogramAlgorithm::lowPassFiltering(double* array,
                int length) {
            double alpha = 0.99;
            for(int i = 1; i < length; i++)
                array[i] = (1.0 - alpha)*array[i] + alpha*array[i-1];
        }

        vector<double> *BeatHistogramAlgorithm::downSampling(double* array,
                int length, int k) {
            auto result = new vector<double>();
            for(int i = 0; i * k < length; i++) {
                result->push_back(array[i * k]);
            }
            return result;
        }

        void BeatHistogramAlgorithm::meanRemoving(vector<double>& vec) {
            double mean = 0.0;
            for(double x: vec)
                mean += x;
            mean /= (double)vec.size();
            for(double &x: vec) {
                x = x - mean;
            }
        }

        vector<double>* BeatHistogramAlgorithm::autoCorrelation(
                vector<double>& vec) {
            auto result = new vector<double>();
            for(int k = 0; k < vec.size(); k++) {
                double sum = 0.0;
                for(int i = k; i < vec.size(); i++)
                    sum += vec[i] * vec[i - k];
                sum /= (double) vec.size();
                result->push_back(sum);
            }
            return result;
        }

        double BeatHistogramAlgorithm::hertzToBpm(double hertz) {
            return hertz * 60;
        }

        vector<pair<double, double> >* BeatHistogramAlgorithm::getPeaks(
                vector<double>& vec, int peaksNum, int sampleRate) {

            double deltaLag = 1000.0 / double(sampleRate);

            vector<pair<double,double> > *mappedAutocor = new vector<pair<double,double> >();
            for(int i = 0; i < vec.size(); i++) {
                double bpm = 60000.0 / (i * deltaLag);
                mappedAutocor->push_back(make_pair(vec[i], bpm));
            }
            sort(mappedAutocor->begin(), mappedAutocor->end(),
                    [](const std::pair<double, double>& first, const std::pair<double, double>& second) {
                        return first.first < second.first;
                    });

            vector<pair<double,double> > *result = new vector<pair<double,double> >();

            int choosedPeaks = 0;
            int idx = mappedAutocor->size() - 1;
            while(choosedPeaks < peaksNum) {
                if(idx == 0)
                    break;
                auto current = mappedAutocor->at(idx);
                if(current.second >= 40 && current.second <= 200
                        && current.first > 0.0) {

                    result->push_back(current);
                    choosedPeaks++;

                }
                idx--;
            }

            mappedAutocor->clear();
            delete mappedAutocor;

            return result;
        }

        double BeatHistogramAlgorithm::BpmToHertz(double bpm) {
            return bpm / 60.0;
        }

        vector<double>* BeatHistogramAlgorithm::extractFeatures(
                double* histogram, unsigned int length) {

            vector<double> *features = new vector<double>;
            vector<pair<double, double> > *mappedHist = new vector<pair<double, double> >();
            for(int i = 0; i < length; i++) {
                mappedHist->push_back(make_pair(histogram[i], i+40));
            }
            sort(mappedHist->begin(), mappedHist->end(),
                    [](const std::pair<double, double>& first, const std::pair<double, double>& second) {
                        return first.first < second.first;
                    });
            double firstPeak = mappedHist->at(mappedHist->size() - 1).first;
            double firstPeakBpm = mappedHist->at(mappedHist->size() - 1).second;
            int idx = mappedHist->size() - 2;
            while(mappedHist->at(idx).second > firstPeakBpm - 5 &&
                    mappedHist->at(idx).second < firstPeakBpm + 5)
                idx--;

            double secondPeak = mappedHist->at(idx).first;
            double secondPeakBpm = mappedHist->at(idx).second;

            mappedHist->clear();
            delete mappedHist;

            features->push_back(firstPeak);
            features->push_back(secondPeak);
            features->push_back(secondPeak / firstPeak);
            features->push_back(firstPeakBpm);
            features->push_back(secondPeakBpm);

            return features;

        }

    } /* namespace algorithms */
} /* namespace audiohash */
