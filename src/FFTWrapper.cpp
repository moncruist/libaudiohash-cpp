/**
 * @file FFTWrapper.cpp
 * @brief Реализация класса FFTWrapper для быстрого преобразования Фурье.
 *
 *  Created on: 28.02.2013
 * @author moncruist
 */

#include <complex>
#include <cstring>
#include <iostream>
#include "FFTWrapper.h"
#include <fftw3.h>
using namespace std;

namespace audiohash {


    /**
     * Инициализация БПФ.
     * @param length
     */
    FFTWrapper::FFTWrapper(unsigned int length) :
        fftLength(length) {
        in = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*fftLength);
        out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*fftLength);

        plan = fftw_plan_dft_1d(fftLength, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    }

    /**
     * Деструктор.
     */
    FFTWrapper::~FFTWrapper() {
        fftw_destroy_plan(plan);
        fftw_free(in);
        fftw_free(out);
    }

    /**
     * Выполняет быстрое преобразование Фурье над
     * действительными числами.
     * @param realBuffer буффер действительных чисел
     * @return буффер комплексных амплитуд. Результат БПФ.
     */
    std::complex<double>* FFTWrapper::execute(double* realBuffer) {

        // Переводим из действительных значений в комплексные
        for(unsigned int i = 0; i < fftLength; i++) {
            in[i][0] = realBuffer[i];
            if(realBuffer[i]== NAN)
                cout << "NAN!!!" << endl;
            in[i][1] = 0.0;
        }

        // Считаем БПФ
        fftw_execute(plan);

        // Подготавливаем результат
        std::complex<double> *result = new std::complex<double>[fftLength];
        for(int i = 0; i < fftLength; i++) {
            result[i].real(out[i][0]);
            result[i].imag(out[i][1]);
        }

        return result;
    }


} /* namespace AudioHash */

