/**
 * @file WindowedAudioData.h
 * @brief Классы для хранения данных
 * @author moncruist
 */

#ifndef WINDOWEDAUDIODATA_H_
#define WINDOWEDAUDIODATA_H_

namespace audiohash {

    /**
     * @brief Класс для хранения однородных данных.
     * @details Класс хранит массив данных поделенных на
     * анализируемые окна фиксированной длины.
     * По сути почти как двумерный массив, только с дополнительными
     * удобными функциями.
     */
	class WindowedAudioData {
	public:
		WindowedAudioData();
		WindowedAudioData(unsigned int windowsCount, unsigned int windowLength,
				const double * const* data, unsigned int sampleRate);
		WindowedAudioData(const WindowedAudioData &data);
		virtual ~WindowedAudioData();

	public:
		double** data() const;
		void setData(unsigned int windowsCount, unsigned int windowLength,
				const double* const* data, unsigned int sampleRate);
		unsigned int windowLength() const;

		unsigned int windowsCount() const;

		unsigned int sampleRate() const;

	public:
		WindowedAudioData &operator=(const WindowedAudioData &d);
	protected:
		void setWindowLength(unsigned int windowLength);
		void setWindowsCount(unsigned int windowsCount);
		void setSampleRate(unsigned int sampleRate);
		void freeData();
		void copyData(const double* const* data);
		void setDataPtr(double** data);

	private:
		double **_data;             //!< массив с данными
		unsigned int _windowsCount; //!< количество окон в массиве
		unsigned int _windowLength; //!< длина каждого окна
		unsigned int _sampleRate;   //!< частота дискретизации сигнала
	};

} /* namespace AudioHash */
#endif /* WINDOWEDAUDIODATA_H_ */
