/**
 * @file AudioFileParser.h
 * @brief Файл реализации парсинга аудио файлов.
 */
#include <sstream>
#include <cmath>
#include <cstring>
#include "AudioFileParser.h"
#include <iostream>

namespace audiohash {

    /**
     * Длина окна по умолчанию. 100мс.
     */
    const int DEFAULT_WINDOW_LENGTH = 100;

    /**
     * Конструктор.
     * Использует идеалогию RAII, т.е. в конструткоре сразу открывается
     * аудиофайл. Использует дли окна DEFAULT_WINDOW_LENGTH
     * @param filename Полное имя аудиофайла для парсинга.
     * @throw AudioFileParserException в случае ошибки доступа к файлу.
     * @sa AudioFileParser(const std::string, int)
     */
    AudioFileParser::AudioFileParser(const std::string &filename)
            throw (AudioFileParserException) :
            AudioFileParser(filename, DEFAULT_WINDOW_LENGTH) {
    }

    /**
     * Конструктор.
     * Использует идеалогию RAII, т.е. в конструткоре сразу открывается
     * аудиофайл.
     * @param filename Полное имя аудиофайла для парсинга.
     * @param windowLength Длина анализируемого окна.
     * @param halfOverlap Перекрывают ли окна друг друга на половину. По умолчанию true.
     * @param lengthInMs Единица измерения windowLength. True - windowLength задан в миллисекундах, false - задан в количестве выборок.
     * @throw AudioFileParserException в случае ошибки доступа к файлу.
     */
    AudioFileParser::AudioFileParser(const std::string &filename,
            int windowLength, const bool halfOverlap, const bool lengthInMs)
                    throw (AudioFileParserException) :
            windowLength(windowLength), halfOverlap(halfOverlap), lengthInMs(
                    lengthInMs) {
        std::stringstream error_msg;
        fileInfo.format = 0;

        file = sf_open(filename.c_str(), SFM_READ, &fileInfo);

        if(file == NULL) {
            error_msg << "Cannot open file: " << filename;
            throw AudioFileParserException(error_msg.str());
        }

        if(fileInfo.channels != 1) {
            error_msg << "Unsupported multichannel file: " << filename;
            sf_close(file);
            throw AudioFileParserException(error_msg.str());
        }
    }

    /**
     * Деструктор. Закрывает аудиофайл (если он был открыт).
     */
    AudioFileParser::~AudioFileParser() throw () {
        if(!file)
            sf_close(file);
    }

    /**
     * Функция парсинга аудио файла.
     * @return Аудиоданные разбитые на окна и отнормализованные.
     */
    WindowedAudioData *AudioFileParser::parse() {
        // Вычисляем сколько значений в одном окне
        // Если lengthInMs == true, то считаем отнисительно частоты дискретизации,
        // иначе просто берем windowLength.
        unsigned int framesInWindow =
                (lengthInMs) ?
                        fileInfo.samplerate * windowLength / 1000 :
                        windowLength;

        //std::cout << fileInfo.samplerate << std::endl;

        // Вычисляем сколько значений в половине окна.
        // Необходимо для того, чтобы знать на сколько
        // перекрывать предыдущее окно и где начинается
        // следующее.
        unsigned int framesInHalfWindow = framesInWindow / 2;

        // Общее количество окон, которые получатся в
        // результате парсинга.
        unsigned int windowsCount;
        if(halfOverlap) {
            windowsCount = ((unsigned int) std::ceil(
                    (long double) fileInfo.frames
                            / (long double) framesInHalfWindow)) - 1;
        } else {
            windowsCount = (unsigned int) std::ceil(
                    (long double) fileInfo.frames / framesInWindow);
        }

        // Считываем все данные разом из аудифайла
        double *allData = new double[fileInfo.frames];
        sf_count_t readed = sf_readf_double(file, allData, fileInfo.frames);

        double **windowedData = new double *[windowsCount];

        if(halfOverlap) {
            // Разбиваем на персекающиеся окна
            long long offset = 0;

            for(int i = 0; i < windowsCount; i++) {
                windowedData[i] = new double[framesInWindow];
                for(int j = 0; j < framesInWindow; j++) {
                    if(j + offset >= fileInfo.frames)
                        windowedData[i][j] = 0;
                    else
                        windowedData[i][j] = allData[j + offset];
                }
                offset += framesInHalfWindow;
            }
        } else {
            for(int i = 0; i < windowsCount; i++) {
                windowedData[i] = new double[framesInWindow];
                long long startIdx = i * framesInWindow;
                for(int j = 0; j < framesInWindow; j++) {
                    if(startIdx + j >= fileInfo.frames) {
                        windowedData[i][j] = 0;
                    } else {
                        windowedData[i][j] = allData[startIdx + j];
                    }
                }
            }
        }

        delete[] allData; // Это уже больше не нужно

        // Подготавливаем результат в виде удобного контейнера
        WindowedAudioData *result = new WindowedAudioData(windowsCount,
                framesInWindow, windowedData, fileInfo.samplerate);

        // Нормализуем данные
        normalizeData(*result);

        // Прибираемся за собой
        for(int i = 0; i < windowsCount; i++)
            delete[] windowedData[i];
        delete[] windowedData;

        return result;
    }

    /**
     * @brief Функция нормализации данных.
     * @details Нормализует данные из аудиофайла. Под нормализацией понимается
     * что все значения будут находиться в пределах от -1.0 до 1.0, относительно
     * максимального по модуля значения. Другими словами, ищем максимальное по модулю
     * значение из всего файла и делим каждое значение на максимальное. Это нужно
     * для того, чтобы данные были инвариантны от громкости.
     * @param data Аудиоданные, разбитые на анализируемые окна.
     */
    void AudioFileParser::normalizeData(WindowedAudioData &data) {
        double **rawData = data.data();
        double max = 0.0;
        for(int i = 0; i < data.windowsCount(); i++) {
            for(int j = 0; j < data.windowLength(); j++) {
                double val = std::abs(rawData[i][j]);
                if(val > max)
                    max = val;
            }
        }

        for(int i = 0; i < data.windowsCount(); i++) {
            for(int j = 0; j < data.windowLength(); j++) {
                rawData[i][j] /= max;
            }
        }
    }
}

