/**
 * @file AudioHasher.h
 * @brief Класс хэширования аудиоданных
 */

#ifndef AUDIOHASHER_H_
#define AUDIOHASHER_H_

#include "algorithms/STFTAlgorithm.h"
#include "algorithms/TimeDomainAlgorithm.h"
#include "algorithms/SpecialAlgorithm.h"
#include "WindowedAudioData.h"
#include "FFTWrapper.h"
#include <vector>

namespace audiohash {

    /**
     * @brief Класс хэширования аудиоданных.
     * @details Класс для вычисления хэша переданных
     * аудиоданных. Гибко конфигурируется на тему используемых
     * алгоритмов для хэширования (они вынесены отдельно).
     */
    class AudioHasher {
    public:
        AudioHasher(WindowedAudioData *data, int windowLengthMs,
                std::vector<algorithms::STFTAlgorithm *> *stftAlgs,
                std::vector<algorithms::TimeDomainAlgorithm *> *timeAlgs,
                std::vector<algorithms::SpecialAlgorithm *> *specialAlgs);
        virtual ~AudioHasher();

        std::vector<std::vector<double>> *calcHash();

    private:
        void applyHammingWindow(double *data, unsigned int length);
        std::vector<double> *calcSTFTHash(const double *window, unsigned int length, int frameRate);
        std::vector<double> *calcTimeHash(const double *window, unsigned int length);
        std::vector<double> *calcSpecialHash(WindowedAudioData *data);
    private:
        WindowedAudioData *raw_data;      //!< сырые данные из аудиофайла
        std::vector<algorithms::STFTAlgorithm *> *stftAlgorithms;   /** Список алгоритмов в частотном домене */
        std::vector<algorithms::TimeDomainAlgorithm *> *timeAlgorithms; /** Список алгоритмов во временном домене */
        std::vector<algorithms::SpecialAlgorithm *> *specialAlgorithms; /** Список специальных алгоритмов */
        int windowLengthMs;     //!< длина окна в мс
        FFTWrapper *fft;        //!< контекст преобразования Фурье.
    };

} /* namespace AudioHash */
#endif /* AUDIOHASHER_H_ */
