/**
 * @file DWTDaubechiesWrapper.cpp
 * @brief Файл реализации класса DWTDaubechiesWrapper.
 *
 * Класс DWTDaubechiesWrapper служит оберткой на функциями из
 * библиотек GNU Scientific Library, производящими
 * дискретное вейвлет-преобразование с вейвлетом Добеши 4 порядка.
 */

#include "DWTDaubechiesWrapper.h"
#include <cstring>
#include <gsl/gsl_wavelet.h>
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

namespace audiohash {

    /**
     * Конструктор.
     * @param dataLength длина буфера данных
     * @param decompositionLevel количество уровней декомпозиции
     */
    DWTDaubechiesWrapper::DWTDaubechiesWrapper(unsigned int dataLength,
            unsigned int decompositionLevel) :
            dataLength(dataLength), decompLevel(decompositionLevel) {
        wavelet = gsl_wavelet_alloc(gsl_wavelet_daubechies, 4);
        work = gsl_wavelet_workspace_alloc(dataLength);
    }

    vector<vector<double> > *DWTDaubechiesWrapper::transform(const double* array, double &smooth) {
        double *result = new double[dataLength];
        memcpy(result, array, sizeof(double) * dataLength);
        gsl_wavelet_transform_forward(wavelet, result, 1, dataLength, work);

        int levelsNum = (int)log2((double)dataLength);
        //int skipLevels = levelsNum - decompLevel;
        //int samplesToSkip = 1 << skipLevels;
        vector<vector<double> > *decomp = new vector<vector<double> >();
        smooth = result[0];
        for(int i = 0; i < decompLevel; i++) {
            int skipSamples = 1 << (i);
            int levelSamples = skipSamples;
            vector<double> levelValues;
            for(int j = 0; j < levelSamples; j++) {
                levelValues.push_back(result[skipSamples + j]);
            }
            decomp->push_back(levelValues);
        }
        delete[] result;
        return decomp;
    }

    /**
     * Фильтрация данных с помощью DWT
     * @param array буффер с данными
     * @return отфильтрованные буферы данных
     */
    vector<double*> DWTDaubechiesWrapper::filter(const double* array) {

        double *transfArr;
        vector<double *> filtered;
        int levelsNum = (int)log2((double)dataLength);

        for(int level = 0; level < decompLevel; level++) {
            transfArr = new double[dataLength];
            memcpy(transfArr, array, sizeof(double) * dataLength);
            gsl_wavelet_transform_forward(wavelet, transfArr, 1, dataLength, work);

            int levelValuesStart = 1 << level;
            int levelValuesEnd = 1 << (level + 1);
            for(int i = 1; i < levelValuesStart; i++)
                transfArr[i] = 0.0;
            for(int i = levelValuesEnd; i < dataLength; i++)
                transfArr[i] = 0.0;
            gsl_wavelet_transform_inverse(wavelet, transfArr, 1, dataLength, work);

            filtered.push_back(transfArr);
        }

        return filtered;
    }

    DWTDaubechiesWrapper::~DWTDaubechiesWrapper() {
        gsl_wavelet_free(wavelet);
        gsl_wavelet_workspace_free(work);
    }

} /* namespace audiohash */
