/**
 * @file WindowedAudioData.cpp
 * @brief Классы для хранения данных
 *  Created on: 31.01.2013
 * @author moncruist
 */
#include <stddef.h>
#include <cstring>
#include "WindowedAudioData.h"

namespace audiohash {

    /**
     * Конструктор по умолчанию
     */
	WindowedAudioData::WindowedAudioData() :
			_windowsCount(0), _windowLength(0),
			_data(NULL), _sampleRate(0) {
	}

	/**
	 * Метод копирует переданные данные в приватные члены
	 * @param data массив с данными
	 */
	void WindowedAudioData::copyData(const double* const* data) {
		_data = new double*[_windowsCount];
		for (unsigned int i = 0; i < _windowsCount; i++) {
			_data[i] = new double[_windowLength];
			std::memcpy(_data[i], data[i], sizeof(double) * _windowLength);
		}
	}

	/**
	 * Параметизированный конструктор
	 * @param windowsCount количество окон
	 * @param windowLength длина окна
	 * @param data источник данных
	 * @param sampleRate частота дискретизации данных
	 */
	WindowedAudioData::WindowedAudioData(unsigned int windowsCount, unsigned int windowLength,
			const double * const*data, unsigned int sampleRate) :
			_windowsCount(windowsCount), _windowLength(windowLength),
			_sampleRate(sampleRate){
		copyData(data);
	}

	/**
	 * Конструктор копирования
	 */
	WindowedAudioData::WindowedAudioData(const WindowedAudioData &data) :
		WindowedAudioData(data._windowsCount, data._windowLength, data._data, data._sampleRate) {
	}

	/**
	 * Метод освобождает занимамую память
	 */
	void WindowedAudioData::freeData() {
		if (!_data) {
			for (int i = 0; i < _windowsCount; i++) {
				delete[] _data[i];
			}
			delete[] _data;
			_data = nullptr;
		}
	}

	/**
	 * Деструктор
	 */
	WindowedAudioData::~WindowedAudioData() {
		freeData();
	}

	/**
	 * Возвращает указатель на данные
	 * @return
	 */
	double** WindowedAudioData::data() const {
		return _data;
	}

	/**
	 * Устанавливает новые данные для хранения.
	 * Старые данные удаляются.
	 * @param windowsCount количество окон
	 * @param windowLength длина окна
	 * @param data данные
	 * @param sampleRate частота дискретизации данных
	 */
    void WindowedAudioData::setData(unsigned int windowsCount,
            unsigned int windowLength, const double* const * data,
            unsigned int sampleRate) {
        freeData();
		setWindowsCount(windowsCount);
		setWindowLength(windowLength);
		setSampleRate(sampleRate);
		copyData(data);
	}

    /**
     * Возвращает длину окна.
     */
	unsigned int WindowedAudioData::windowLength() const {
		return _windowLength;
	}

	void WindowedAudioData::setWindowLength(unsigned int windowLength) {
		_windowLength = windowLength;
	}

	unsigned int WindowedAudioData::windowsCount() const {
		return _windowsCount;
	}

	void WindowedAudioData::setWindowsCount(unsigned int windowsCount) {
		_windowsCount = windowsCount;
	}

	/**
	 * Перегруженный оператор присваивания. Копирует данные операнда.
	 */
	WindowedAudioData &WindowedAudioData::operator=(const WindowedAudioData &d) {
		if(this == &d)
			return *this;
		setData(d._windowsCount, d._windowLength, d._data, d._sampleRate);
		return *this;
	}

    unsigned int WindowedAudioData::sampleRate() const {
        return _sampleRate;
    }

    void WindowedAudioData::setSampleRate(unsigned int sampleRate) {
        _sampleRate = sampleRate;
    }

	void WindowedAudioData::setDataPtr(double ** data) {
		_data = data;
	}

} /* namespace AudioHash */
