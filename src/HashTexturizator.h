/*
 * TexturizeHash.h
 *
 *  Created on: 14.05.2013
 *      Author: moncruist
 */

#ifndef HASHTEXTURIZATOR_H_
#define HASHTEXTURIZATOR_H_

#include <vector>

namespace audiohash {

    class HashTexturizator {
    public:
        HashTexturizator(int hashWindowLength, int mfccCoefNum);
        std::vector<std::vector<double> > *texturize(std::vector<std::vector<double> > &hash);
        virtual ~HashTexturizator();

    private:
        int hashWindowLen;
        int mfccCoefs;
    };

} /* namespace audiohash */
#endif /* HASHTEXTURIZATOR_H_ */
