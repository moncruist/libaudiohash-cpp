/*
 * AudioFileParserException.cpp
 *
 *  Created on: 31.01.2013
 *      Author: moncruist
 */

#include "AudioFileParserException.h"

namespace audiohash {


AudioFileParserException::AudioFileParserException(const std::string &message) throw() :
		msg(message) {
}

const char *AudioFileParserException::what() const throw() {
	return msg.c_str();
}

}
