/*
 * AudioHashWrapper.cpp
 *
 *  Created on: 17.04.2013
 *      Author: moncruist
 */

#include <jni.h>
#include <vector>
#include "AudioHashWrapper.h"
#include "../AudioHasher.h"
#include "../AudioFileParser.h"
#include "../algorithms/STFTAlgorithm.h"
#include "../WindowedAudioData.h"
#include "../algorithms/MFCCAlgorithm.h"
#include "../algorithms/SpectralCentroidAlgorithm.h"
#include "../algorithms/SpectralRolloffAlgorithm.h"
#include "../algorithms/SpectralFluxAlgorithm.h"
#include "../algorithms/TimeDomainAlgorithm.h"
#include "../algorithms/TimeZeroCrossingsAlgorithm.h"
#include "../algorithms/SpecialAlgorithm.h"
#include "../algorithms/BeatHistogramAlgorithm.h"
#include "../HashTexturizator.h"

using namespace std;
using namespace audiohash;
using namespace audiohash::algorithms;

extern "C" {
    JNIEXPORT jobjectArray JNICALL Java_com_moncruist_audiohash_AudioHashWrapper_calcSpectralHash
      (JNIEnv *env, jclass klass, jstring fileName) {
        const char *file = env->GetStringUTFChars(fileName, 0);
        try {
            AudioFileParser parser(file);
            WindowedAudioData *data = parser.parse();
            vector<STFTAlgorithm *> stftAlgs;
            vector<TimeDomainAlgorithm *> timeAlgs;
            stftAlgs.push_back(new MFCCAlgorithm(20));

            stftAlgs.push_back(new SpectralCentroidAlgorithm());
            stftAlgs.push_back(new SpectralRolloffAlgorithm());

            stftAlgs.push_back(new SpectralFluxAlgorithm());

            timeAlgs.push_back(new TimeZeroCrossingsAlgorithm());

            AudioHasher hasher(data, DEFAULT_WINDOW_LENGTH, &stftAlgs, &timeAlgs, nullptr);
            auto specHashes = hasher.calcHash();
            HashTexturizator texture(DEFAULT_WINDOW_LENGTH, 20);
            auto hashes = texture.texturize(*specHashes);

            delete specHashes;

            int hashLength = hashes->size();
            int vectorLength = 0;
            if(hashLength)
                vectorLength = hashes->at(0).size();

            jclass doubleArrayClass = env->FindClass("[D");

            if(doubleArrayClass == NULL) {
                return NULL;
            }

            jobjectArray resultArray = env->NewObjectArray((jsize)hashLength, doubleArrayClass, NULL);
            for(int i = 0; i < hashes->size(); i++) {
                double *arr = new double[vectorLength];
                for(int j = 0; j < hashes->at(i).size(); j++)
                    arr[j] = hashes->at(i)[j];
                jdoubleArray jarr = env->NewDoubleArray((jsize)vectorLength);
                env->SetDoubleArrayRegion(jarr, (jsize) 0, (jsize)vectorLength, (jdouble *)arr);
                env->SetObjectArrayElement(resultArray, (jsize)i, jarr);
                env->DeleteLocalRef(jarr);
                delete[] arr;
            }

            for(int i = 0; i < stftAlgs.size(); i++)
                delete stftAlgs[i];
            stftAlgs.clear();

            for(int i = 0; i < timeAlgs.size(); i++)
                delete timeAlgs[i];
            timeAlgs.clear();

            delete data;

            env->ReleaseStringUTFChars(fileName, file);

            return resultArray;

        } catch (...) {
            env->ReleaseStringUTFChars(fileName, file);
            return NULL;
        }
    }

    JNIEXPORT jdoubleArray JNICALL Java_com_moncruist_audiohash_AudioHashWrapper_calcBeatHistogram
      (JNIEnv *env, jclass, jstring fileName) {
        const char *file = env->GetStringUTFChars(fileName, 0);
        try {
            AudioFileParser parser(file, 1<<16, false, false);
            WindowedAudioData *data = parser.parse();
            vector<SpecialAlgorithm *> specialAlgs = {
                    new BeatHistogramAlgorithm()
            };
            AudioHasher hasher(data, DEFAULT_WINDOW_LENGTH, nullptr,
                    nullptr, &specialAlgs);
            auto beats = hasher.calcHash();

            int hashLength = beats->at(0).size();

            double *arr = new double[hashLength];
            for(int i = 0; i < hashLength; i++)
                arr[i] = beats->at(0).at(i);

            jdoubleArray jresult = env->NewDoubleArray((jsize) hashLength);
            env->SetDoubleArrayRegion(jresult, (jsize) 0, (jsize) hashLength,
                                    (jdouble *) arr);

            delete data;
            delete specialAlgs.at(0);
            delete beats;
            delete[] arr;

            env->ReleaseStringUTFChars(fileName, file);

            return jresult;

        } catch(...) {
            env->ReleaseStringUTFChars(fileName, file);
            return NULL;
        }
    }
}
