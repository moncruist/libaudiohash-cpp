/**
 * @file FFTWrapper.h
 * @brief Заголовочный файл класса FFTWrapper.
 *
 * Класс FFTWrapper служит оберткой на функциями из
 * библиотек FFTW, производящими
 * преобразование Фурье.
 */

#ifndef FFTWRAPPER_H_
#define FFTWRAPPER_H_

#include <complex>
#include <fftw3.h>

namespace audiohash {

    /**
     * @brief Класс для Быстрого Преобразования Фурье.
     * @details Класс использует библиотечные функции FFTW
     * для преобразования Фурье и адаптирован для использования со стандартной
     * библиотекой C++.
     */
	class FFTWrapper {
	public:
	    explicit FFTWrapper(unsigned int length);
	    ~FFTWrapper();
	    std::complex<double> *execute(double *realBuffer);
	private:
		unsigned int fftLength;
		fftw_complex *in;
		fftw_complex *out;
		fftw_plan plan;
	};

} /* namespace AudioHash */
#endif /* FFTWRAPPER_H_ */
