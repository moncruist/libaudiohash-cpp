/*
 * TexturizeHash.cpp
 *
 *  Created on: 14.05.2013
 *      Author: moncruist
 */

#include "HashTexturizator.h"

#include <cstring>
#include <iostream>

using namespace std;

namespace audiohash {

    HashTexturizator::HashTexturizator(int hashWindowLength, int mfccCoefNum) :
        hashWindowLen(hashWindowLength), mfccCoefs(mfccCoefNum) {
    }

    vector<vector<double> >* HashTexturizator::texturize(vector<vector<double> >& hash) {
        vector<vector<double> > *textureHash = new vector<vector<double> >();
        if(hash.size() == 0)
            return textureHash;
        int hashLength = hash[0].size();
        int textureWindow = 500 / hashWindowLen * 2;

        int hashCount = hash.size() / textureWindow;
        if(hash.size() % textureWindow)
            hashCount++;

        int mfccTextureCoef = 10;
        int otherCoeffs = (hashLength - mfccCoefs) > 0 ? hashLength - mfccCoefs : 0;

        for(int i = 0; i < hashCount; i++) {
            double *means = new double[hashLength];
            double *variances = new double[hashLength];

            memset(means, 0, sizeof(double) * hashLength);
            memset(variances, 0, sizeof(double) * hashLength);

            int currentHashFrames = ((i+1)*textureWindow) >= hash.size() ?
                    hash.size() - i*textureWindow :
                    textureWindow;

            for(int j = 0; j < currentHashFrames; j++) {
                for(int k = 0; k < hashLength; k++) {
                    means[k] += hash[i*textureWindow + j][k];
                }
            }

            for(int k = 0; k < hashLength; k++) {
                means[k] /= currentHashFrames;
            }


            for(int j = 0; j < currentHashFrames; j++) {
                for(int k = 0; k < hashLength; k++) {
                    double val = hash[i*textureWindow + j][k] - means[k];
                    variances[k] += val*val;
                }
            }

            for(int k = 0; k < hashLength; k++) {
                variances[k] /= currentHashFrames;
            }

            vector<double> frame;
            for(int n = 0; n < mfccTextureCoef; n++) {
                frame.push_back(means[n]);
                //frame.push_back(variances[n]);
            }
            for(int n = mfccCoefs; n < mfccCoefs + otherCoeffs; n++) {
                frame.push_back(means[n]);
                //frame.push_back(variances[n]);
            }

            textureHash->push_back(frame);

            delete[] means;
            delete[] variances;
        }

        return textureHash;
    }

    HashTexturizator::~HashTexturizator() {
        // TODO Auto-generated destructor stub
    }

} /* namespace audiohash */
