/*
 * main.cpp
 * Главный файл программы.
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <sndfile.h>
#include <ctime>
#include "AudioFileParser.h"
#include "WindowedAudioData.h"
#include "DWTDaubechiesWrapper.h"
#include "FFTWrapper.h"
#include "AudioHasher.h"
#include "HashTexturizator.h"
#include "algorithms/MFCCAlgorithm.h"
#include "algorithms/SpectralCentroidAlgorithm.h"
#include "algorithms/SpectralRolloffAlgorithm.h"
#include "algorithms/SpectralFluxAlgorithm.h"
#include "algorithms/TimeDomainAlgorithm.h"
#include "algorithms/TimeZeroCrossingsAlgorithm.h"
#include "algorithms/BeatHistogramAlgorithm.h"

#include <boost/program_options.hpp>
using namespace std;
using namespace audiohash;
using namespace audiohash::algorithms;

//#define TEST_BEAT

namespace po = boost::program_options;

const char *sample_file = "build/sample3.wav";

po::options_description *generate_options() {
    po::options_description *desc = new po::options_description(
            "Allowed options");
    desc->add_options()
            ("help", "produce help message")
            ("file,f",po::value<string>(), "file to parse")
            ("verbose,v",po::value<bool>()->implicit_value(true), "be verbose")
            ("output,o", po::value<string>(), "output file to store hash")
            ("beat-histogram", "calculate Beat Histogram Features");
    return desc;
}

int main(int argc, char *argv[]) {

    po::options_description *desc = generate_options();

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, *desc), vm);
    po::notify(vm);

    if(vm.count("help")) {
        cout << *desc << endl;
        return 0;
    }

    bool exit_flag = false;

    string fileName, outputFileName;

    bool verbose = false;
    bool noOverlap = false;
    bool lengthInMs = true;
    bool beatHistogram;

    if(vm.count("verbose"))
        verbose = true;

    if(!vm.count("file")) {
        cout << "Input file was not set. Aborting" << endl;
        exit_flag = true;
    } else {
        fileName = vm["file"].as<string>();
    }

    if(!exit_flag) {
        if(!vm.count("output")) {
            if(verbose)
                cout << "Output file was not set. Using file hash.txt" << endl;
            outputFileName = "hash.txt";
        } else {
            outputFileName = vm["output"].as<string>();
            if(verbose)
                cout << "Output file was set to " << outputFileName << endl;
        }

        int windowLength = 100;


        vector<STFTAlgorithm *> stftAlgs = {
                new MFCCAlgorithm(20),
                new SpectralCentroidAlgorithm(),
                new SpectralRolloffAlgorithm(),
                new SpectralFluxAlgorithm()
        };
        vector<TimeDomainAlgorithm *> timeAlgs = {
                new TimeZeroCrossingsAlgorithm()
        };

        if(vm.count("beat-histogram")) {
            beatHistogram = true;
            if(verbose)
                cout << "Beat Histogram is enabled" << endl;
        } else {
            beatHistogram = false;
        }

        vector<vector<double>> *hash;

        if(beatHistogram) {
            if(verbose)
                cout << "Starting parsing file " << fileName << endl;
            AudioFileParser beatParser(fileName, 1<<16, false, false);
            WindowedAudioData *windowed = beatParser.parse();
            if(verbose)
                cout << "Parsing complete" << endl;

            if(verbose)
                cout << "Starting hashing file " << fileName << endl;
            std::clock_t begin = std::clock();
            vector<SpecialAlgorithm *> specialAlgs = {new BeatHistogramAlgorithm()};
            AudioHasher beatHasher(windowed, DEFAULT_WINDOW_LENGTH, nullptr, nullptr, &specialAlgs);
            hash = beatHasher.calcHash();
            delete windowed;

            if(verbose)
                cout << "Hashing complete" << endl;
            std::clock_t end = std::clock();
            if(verbose)
                std::cout << "Hashing time " << double(end - begin) / CLOCKS_PER_SEC
                        << std::endl;
        } else {
            if(verbose)
                cout << "Starting parsing file " << fileName << endl;
            AudioFileParser parser(fileName, DEFAULT_WINDOW_LENGTH);
            WindowedAudioData *windowed = parser.parse();
            if(verbose)
                cout << "Parsing complete" << endl;

            if(verbose)
                cout << "Starting hashing file " << fileName << endl;
            std::clock_t begin = std::clock();
            AudioHasher hasher(windowed, DEFAULT_WINDOW_LENGTH, &stftAlgs, &timeAlgs, nullptr);
            auto specHash = hasher.calcHash();

            HashTexturizator texture(DEFAULT_WINDOW_LENGTH, 20);
            hash = texture.texturize(*specHash);
            delete specHash;
            delete windowed;


            if(verbose)
                cout << "Hashing complete" << endl;
            std::clock_t end = std::clock();
            if(verbose)
                std::cout << "Hashing time " << double(end - begin) / CLOCKS_PER_SEC
                        << std::endl;
        }


        ofstream output;
        output.open(outputFileName, ios::out | ios::trunc);

        for(auto vec : *hash) {
            for(int i = 0; i < vec.size(); i++) {
                output << vec[i];
                if(i < vec.size() - 1)
                    output << " ";
                else
                    output << endl;
            }
        }

        output.close();

        if(verbose)
            cout << "Writed hash to file " << outputFileName << endl;
    }

#ifdef TEST_BEAT
    string sampleName = "build/sample1.wav";
    string outputFile = "test_dwt.txt";
    AudioFileParser parser1(sampleName, 1<<16, false, false);
    WindowedAudioData *windowed = parser1.parse();

    vector<SpecialAlgorithm *> specialAlgs = {new BeatHistogramAlgorithm()};
    AudioHasher hasher1(windowed, DEFAULT_WINDOW_LENGTH, nullptr, nullptr, &specialAlgs);
    auto hash = hasher1.calcHash();

    cout << hash->size() << endl;
    for(auto v : *hash) {
        for(auto g : v)
            cout << g << " ";
        cout << endl;
    }


    cout << "test" << endl;
#endif
    return 0;

}

