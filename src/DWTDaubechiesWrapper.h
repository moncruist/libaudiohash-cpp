/**
 * @file DWTDaubechiesWrapper.h
 * @brief Заголовочный файл класса DWTDaubechiesWrapper.
 *
 * Класс DWTDaubechiesWrapper служит оберткой на функциями из
 * библиотек GNU Scientific Library, производящими
 * дискретное вейвлет-преобразование с вейвлетом Добеши 4 порядка.
 */
#ifndef DWTWRAPPER_H_
#define DWTWRAPPER_H_

#include <gsl/gsl_wavelet.h>
#include <vector>

namespace audiohash {

    /**
     * @brief Класс для дискретного вейвлет-преобразования.
     * @details Класс использует библиотечные функции Gnu Scientific Library
     * для дискретного вейвлет-преобразования Добеши 4 и
     * адаптирован для использования со стандартной
     * библиотекой C++.
     */
    class DWTDaubechiesWrapper {
    public:
        DWTDaubechiesWrapper(unsigned int dataLength, unsigned int decompositionLevel);

        std::vector<std::vector<double> > *transform(const double *array, double &smooth);
        std::vector<double *> filter(const double *array);

        virtual ~DWTDaubechiesWrapper();
    private:

        unsigned int dataLength;
        unsigned int decompLevel;   //!< количество уровней декомпозиции
        gsl_wavelet *wavelet;
        gsl_wavelet_workspace *work;
    };

} /* namespace audiohash */
#endif /* DWTWRAPPER_H_ */
