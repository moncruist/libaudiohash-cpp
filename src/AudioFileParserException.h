/**
 * @file AudioFileParserException.h
 * @brief Определение исключени при парсинге файлов
 *  Created on: 31.01.2013
 * @author moncruist
 */

#ifndef AUDIOFILEPARSEREXCEPTION_H_
#define AUDIOFILEPARSEREXCEPTION_H_

#include <exception>
#include <string>


namespace audiohash {

	class AudioFileParserException : std::exception {
	public:
		AudioFileParserException(const std::string &message) throw();
		const char *what() const throw();
	private:
		std::string msg;
	};

}
#endif /* AUDIOFILEPARSEREXCEPTION_H_ */
