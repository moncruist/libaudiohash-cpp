/**
 * @file AudioHasher.cpp
 * @brief Класс хэширования аудиоданных
 */

#include <cstring>
#include <cmath>
#include <vector>
#include <complex>
#include "WindowedAudioData.h"
#include "FFTWrapper.h"
#include "AudioHasher.h"

#include <iostream>

using namespace std;
using namespace audiohash::algorithms;

namespace audiohash {

    /**
     * Конструктор.
     * @param data Данные, для которых нужно вычислить хэш.
     * @param windowLengthMs Длина каждого окна в миллисекундах
     * @param stftAlgs Набор алгоритмов, основанных на БПФ, для хэширования
     */
    AudioHasher::AudioHasher(WindowedAudioData *data, int windowLengthMs,
            vector<STFTAlgorithm *> *stftAlgs,
            std::vector<algorithms::TimeDomainAlgorithm *> *timeAlgs,
            std::vector<algorithms::SpecialAlgorithm *> *specialAlgs) :
            raw_data(data), windowLengthMs(windowLengthMs), stftAlgorithms(
                    stftAlgs), timeAlgorithms(timeAlgs), fft(nullptr), specialAlgorithms(
                    specialAlgs) {
    }

    AudioHasher::~AudioHasher() {
    }

    /**
     * @brief Функция вычисления хэша.
     * @details Функция вычисляет хэш данных при помощи заданных
     * алгоритмов. Для алгоритмов на основе БПФ она подготовливает
     * данные для алгоритмов путем применения БПФ (сами алгоритмы не
     * выполняют БПФ в целях именешения вычислительных расходов).
     * @return Хэш аудиоданных
     */
    vector<vector<double>> *AudioHasher::calcHash() {
        int windows_count = raw_data->windowsCount();
        int window_length = raw_data->windowLength();

        // Восстанавливаем частоту дискретизации данных
        int frameRate = raw_data->sampleRate();

        // Здесь будут храниться хэши
        auto hashes = new vector<vector<double> >();

        if(stftAlgorithms) {
            fft = new FFTWrapper(window_length);
        }

        // Алгоритмы прогоняются каждый раз для каждого окна
        if(stftAlgorithms != nullptr || timeAlgorithms != nullptr) {
            for(int window_idx = 0; window_idx < windows_count; window_idx++) {
                vector<double> windowHash;

                if(stftAlgorithms) {
                    auto coeffs = calcSTFTHash(raw_data->data()[window_idx],
                            window_length, frameRate);

                    for(double coeff : *coeffs) {
                        windowHash.push_back(coeff);
                    }

                    delete coeffs;
                }
                if(timeAlgorithms) {
                    auto coeffs = calcTimeHash(raw_data->data()[window_idx],
                            window_length);

                    for(double coeff : *coeffs) {
                        windowHash.push_back(coeff);
                    }

                    delete coeffs;
                }

                hashes->push_back(windowHash);
            }
        }

        if(specialAlgorithms) {
            vector<double> specHash;
            auto coeffs = calcSpecialHash(raw_data);

            for(double coeff : *coeffs) {
                specHash.push_back(coeff);
            }

            delete coeffs;
            hashes->push_back(specHash);
        }

        if(fft)
            delete fft;
        return hashes;

    }

    /**
     * Функция для применения взвешивающего окна Хэмминга.
     * @param data Исходные данные
     * @param length Длина данных
     */
    void AudioHasher::applyHammingWindow(double* data, unsigned int length) {
        for(int i = 0; i < length; i++) {
            double hamming = 0.54
                    - 0.46 * std::cos(2 * M_PI * i / (double) (length - 1));
            data[i] *= hamming;
        }
    }

    /**
     * Функция прогоняет данные окна через частотные алгоритмы.
     * @param window данные окна
     * @param length длина окна
     * @param frameRate частота дискретизации окна
     * @return Частотные коэффициенты
     */
    vector<double>* AudioHasher::calcSTFTHash(const double *window,
            unsigned int length, int frameRate) {
        // Копируем окно
        double *workWindow = new double[length];
        memcpy(workWindow, window, sizeof(double) * length);
        // Применяем окно Хэмминга
        applyHammingWindow(workWindow, length);

        // Применяем БПФ
        unsigned int fft_length = length;
        auto fft_result = fft->execute(workWindow); ;

        vector<double> *allCoefs = new vector<double>(); // Вектор коэффициентов для текущего окна
        // Получаем их при помощи алгоритмов
        for(STFTAlgorithm *alg : *stftAlgorithms) {
            // Устанавливаем данные для алгоритма и применяем его
            alg->setData(fft_result, fft_length, frameRate, windowLengthMs);
            auto coeffs = alg->computeHash();

            // Копируем коэффициенты в общую кучу
            allCoefs->insert(allCoefs->end(), coeffs->begin(), coeffs->end());
            delete coeffs;
        }
        delete[] workWindow;
        delete[] fft_result;
        return allCoefs;
    }

    /**
     * Функция прогоняет данные через временные алгоритмы
     * @param window данные окна
     * @param length длина окна
     * @return Временные коэффициенты
     */
    vector<double>* AudioHasher::calcTimeHash(const double *window,
            unsigned int length) {
        // Копируем окно
        double *workWindow = new double[length];
        memcpy(workWindow, window, sizeof(double) * length);

        vector<double> *allCoefs = new vector<double>(); // Вектор коэффициентов для текущего окна

        // Получаем их при помощи алгоритмов
        for(TimeDomainAlgorithm *alg : *timeAlgorithms) {
            // Устанавливаем данные для алгоритма и применяем его
            alg->setData(workWindow, length, windowLengthMs);
            auto coeffs = alg->computeHash();

            // Копируем коэффициенты в общую кучу
            allCoefs->insert(allCoefs->end(), coeffs->begin(), coeffs->end());
            delete coeffs;
        }
        delete[] workWindow;
        return allCoefs;
    }

    /**
     * Функция прогоняет данные через специальные алгоритмы.
     * Данные передаются все сразом, без подготовки, так как
     * таким алгоритмам как правило нужен большой обзор.
     * @param data данные аудиофайла
     * @return Значения специальных алгоритмов
     */
    std::vector<double>* AudioHasher::calcSpecialHash(WindowedAudioData* data) {
        vector<double> *allCoefs = new vector<double>();
        for(SpecialAlgorithm *alg : *specialAlgorithms) {
            // Устанавливаем данные для алгоритма и применяем его
            alg->setData(data);
            auto coeffs = alg->computeHash();

            // Копируем коэффициенты в общую кучу
            allCoefs->insert(allCoefs->end(), coeffs->begin(), coeffs->end());
            delete coeffs;
        }
        return allCoefs;
    }

} /* namespace AudioHash */
