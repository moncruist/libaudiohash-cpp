/**
 * @file AudioFileParser.h
 * @brief Заголовочный файл для парсинга аудио файлов.
 */

#ifndef AUDIOFILEPARSER_H_
#define AUDIOFILEPARSER_H_

#include <string>
#include <exception>
#include <sndfile.h>

#include "WindowedAudioData.h"
#include "AudioFileParserException.h"

namespace audiohash {

    extern const int DEFAULT_WINDOW_LENGTH;

    /**
     * @brief Класс для парсинга аудиофайлов.
     * @details Класс AudioFileParser считывает аудиофайл и
     * разбивает его на анализируемые окна. Умеет читать только WAV моно файлы.
     * Окна на половину перекрывают друг друга.
     */
    class AudioFileParser {
    public:
        explicit AudioFileParser(const std::string &filename)
                throw (AudioFileParserException);
        AudioFileParser(const std::string &filename, const int windowLength,
                const bool halfOverlap = true, const bool lengthInMs = true)
                        throw (AudioFileParserException);

        WindowedAudioData *parse();

        ~AudioFileParser() throw ();
    private:
        void normalizeData(WindowedAudioData &data);
    private:
        SF_INFO fileInfo;   //!< информация об аудиофайле
        SNDFILE *file;      //!< дискриптор аудиофайла
        int windowLength; //!< длина анализируемого окна.
        bool halfOverlap;   //!< нужно ли делать перекрывающиеся окна
        bool lengthInMs;    //!< длина окна задана в мс

    };

}

#endif /* AUDIOFILEPARSER_H_ */
