/*
 * audiohash.c
 *
 *  Created on: 24.04.2013
 *      Author: moncruist
 */

#include "audiohash.h"


#include <vector>
#include <string>
#include "../AudioHasher.h"
#include "../AudioFileParser.h"
#include "../algorithms/STFTAlgorithm.h"
#include "../WindowedAudioData.h"
#include "../algorithms/MFCCAlgorithm.h"
#include "../algorithms/SpectralCentroidAlgorithm.h"
#include "../algorithms/SpectralRolloffAlgorithm.h"
#include "../algorithms/SpectralFluxAlgorithm.h"
#include "../algorithms/TimeDomainAlgorithm.h"
#include "../algorithms/TimeZeroCrossingsAlgorithm.h"

using namespace std;
using namespace audiohash;
using namespace audiohash::algorithms;


extern "C" {
    AudioHashResult *audiohash_calc_hash(const char* file_name) {
        AudioHashResult *result = NULL;
        try {
            AudioFileParser parser(file_name);
            WindowedAudioData *data = parser.parse();
            vector<STFTAlgorithm *> stftAlgs = {
                    new MFCCAlgorithm(20),
                    new SpectralCentroidAlgorithm(),
                    new SpectralRolloffAlgorithm(),
                    new SpectralFluxAlgorithm()
            };
            vector<TimeDomainAlgorithm *> timeAlgs = {
                    new TimeZeroCrossingsAlgorithm()
            };

            AudioHasher hasher(data, DEFAULT_WINDOW_LENGTH, &stftAlgs, &timeAlgs, nullptr);
            auto hashes = hasher.calcHash();

            int hashLength = hashes->size();
            int vectorLength = 0;
            if(hashLength)
            vectorLength = hashes->at(0).size();

            result = new AudioHashResult();
            result->hashes_count = hashLength;
            result->hash_length = vectorLength;
            result->hash = new double*[hashLength];
            for(int i = 0; i < hashLength; i++) {
                result->hash[i] = new double[vectorLength];
                for(int j = 0; j < vectorLength; j++) {
                    result->hash[i][j] = hashes->at(i).at(j);
                }
            }

            for(int i = 0; i < stftAlgs.size(); i++)
                delete stftAlgs[i];
            stftAlgs.clear();

            for(int i = 0; i < timeAlgs.size(); i++)
                delete timeAlgs[i];
            timeAlgs.clear();

            delete data;
            for(int i = 0; i < hashes->size(); i++)
                hashes->at(i).clear();
            hashes->clear();
            delete hashes;

            return result;

        } catch (...) {
            if(result) {
                for(int i = 0; i < result->hashes_count; i++) {
                    if(result->hash[i])
                        delete[] result->hash[i];
                }
                delete[] result->hash;
                delete result;
            }
            return NULL;
        }
    }


void audiohash_free_result(AudioHashResult* result) {
    //if(!result) return;
    for(int i = 0; i < result->hashes_count; i++) {
        delete[] result->hash[i];
    }
    delete[] result->hash;
    delete result;
}

}
