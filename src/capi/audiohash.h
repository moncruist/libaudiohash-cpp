/*
 * audiohash.h
 *
 *  Created on: 24.04.2013
 *      Author: moncruist
 */

#ifndef AUDIOHASH_H_
#define AUDIOHASH_H_

extern "C" {

typedef struct _AudioHashResult {
    unsigned int hashes_count;
    unsigned int hash_length;
    double **hash;
}AudioHashResult;


AudioHashResult *audiohash_calc_hash(const char* file_name);
void audiohash_free_result(AudioHashResult *result);

}


#endif /* AUDIOHASH_H_ */
